var Boot = {

    preload: function() {
        //game.load.image('preload', 'assets/preloader.png'); 
    },
  
    create: function() {
        game.input.maxPointers = 1;
        game.state.start('Preloader'); 
    }
};

var Preloader = {
    ready: false, 
  
    preload: function() {
        //this.title = game.add.text(330, 170, 'doodle shoot\n', {font: '80px monospace', fill: '#CC0000', align: 'center'});
        //this.preloadBar = game.add.sprite(330, 170, 'preload');
        //game.load.setPreloadSprite(this.preloadBar);
        game.load.audio('bgm', 'assets/bgm.wav');
        game.load.audio('explosionBgm', 'assets/explosion.wav');
    },
    update: function() {
        //  Make sure all mp3s have been decoded before starting the game
        if (this.ready) {
          return;
        }
        if (game.cache.isSoundDecoded('bgm') && game.cache.isSoundDecoded('explosionBgm')) {
          this.ready = true;
          game.state.start('Menu');
        }
    }
};

var bgm, explosionBgm;
var score;
var livingEnemies=[];
var isPaused, isAwarded, isBossKilled;

var Menu = {
    preload: function() {

        game.load.image('startBackground', 'assets/startBackground.png');
        this.startButton = game.load.image('playButton', 'assets/playButton.png');
        this.scoreButton = game.load.image('scoreButton', 'assets/scoreButton.png');

        // Loat game sprites.
        game.load.image('firstBackground', 'assets/background1.png');
        game.load.image('secondBackground', 'assets/background1.png');
        game.load.image('ground', 'assets/ground.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('bullet', 'assets/bullet.png');
        game.load.image('enemy', 'assets/enemy1.png');



        /// Load block spritesheet.
        //game.load.spritesheet('block1', 'assets/block1.png', 28, 28);
        game.load.spritesheet('block2', 'assets/block2.png', 28, 28);
        

        /// ToDo 1: Load spritesheet
        ///      The name of sprite is 'player'.
        ///      The spritesheet filename is assets/MARIO.png
        ///      A frame size is 32 x 54.
        game.load.spritesheet('player', 'assets/spacecraft.png', 50, 50);
        //game.load.image('player', 'assets/spacecraft.png');

        game.load.spritesheet('explosion', 'assets/explosion.png', 32, 32);

        game.load.image('pauseButton', 'assets/pauseButton.png');

        game.load.image('hp', 'assets/hp.png');

        game.load.image('skill', 'assets/bulletBurst.png');

        game.load.image('volumeUpButton', 'assets/volumeUpButton.png');
        game.load.image('volumeDownButton', 'assets/volumeDownButton.png');

        game.load.image('helper', 'assets/helper.png');
        game.load.image('boss', 'assets/boss.png');

        game.load.image('award', 'assets/award.png');
    },
    create: function() {
        bgm = game.add.audio('bgm');
        bgm.play();
        bgm.loopFull(0.6);

        explosionBgm = game.add.audio('explosionBgm');

        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'startBackground');
        this.startButton.game.add.button(240, 250, 'playButton', this.actionOnClick, this);
        this.scoreButton.game.add.button(240, 360, 'scoreButton', this.scoreOnClick, this);
        // button.onInputOver.add(over, this);
        // button.onInputOut.add(out, this);
        // button.onInputUp.add(up, this);
        this.cursors = game.input.keyboard.createCursorKeys();
        this.title = game.add.text(330, 185, 'doodle shoot\n', {font: '80px monospace', fill: '#CC0000', align: 'center'});
        this.title.anchor.setTo(0.5);

        score=0;
        livingEnemies=[];
        isAwarded=false;
        isBossKilled=false;
    },
    update: function() {
    },
    actionOnClick: function() {
        game.state.start('Main');
    },
    scoreOnClick: function() {
        game.state.start('ScoreBoard');
    }
};

var Main = {
    preload: function() {

    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        //game.add.image(0, 0, 'background'); 
        this.firstBackground = game.add.tileSprite(0, 0, 650, 3000, 'firstBackground');

        /// ToDo 2: How can we enable physics in Phaser? (Mode : ARCADE)

        ///

        game.physics.startSystem(Phaser.Physics.ARCADE);

        game.renderer.renderSession.roundPixels = true;

        this.cursor = game.input.keyboard.createCursorKeys();
        
        this.player = game.add.sprite(game.width/2-20, game.height/2+150, 'player');
        this.player.life = 3;
        score = 0;
        this.enemyKilledCount = 0;
        this.player.facingLeft = false;

        this.helper = game.add.sprite(game.width/2+40, game.height/2+155, 'helper');

        this.award = game.add.sprite(game.width/2, game.height/2, 'award');
        this.award.kill();

        this.boss1 = game.add.sprite(game.width/2-40, 10, 'boss');
        this.boss1.kill();


        this.bulletPool = game.add.group();
        this.bulletPool.enableBody = true;
        this.bulletPool.createMultiple(1000, 'bullet');
        this.bulletPool.setAll('anchor.x', 0.5);
        this.bulletPool.setAll('anchor.y', 0.5);
        this.bulletPool.setAll('outOfBoundsKill', true);
        this.bulletPool.setAll('checkWorldBounds', true);

        this.nextShotAt = 0;
        this.shotDelay = 100;

        this.skillPool = game.add.group();
        this.skillPool.enableBody = true;
        this.skillPool.createMultiple(1000, 'skill');
        this.skillPool.setAll('anchor.x', 0.5);
        this.skillPool.setAll('anchor.y', 0.5);
        this.skillPool.setAll('outOfBoundsKill', true);
        this.skillPool.setAll('checkWorldBounds', true);

        this.skillNextShotAt = 0;
        this.skillShotDelay = 100;

        this.helperBulletPool = game.add.group();
        this.helperBulletPool.enableBody = true;
        this.helperBulletPool.createMultiple(50, 'bullet');
        this.helperBulletPool.setAll('anchor.x', 0.5);
        this.helperBulletPool.setAll('anchor.y', 0.5);
        this.helperBulletPool.setAll('outOfBoundsKill', true);
        this.helperBulletPool.setAll('checkWorldBounds', true);

        this.helperNextShotAt = 0;
        this.helperShotDelay = 500;

        // this.awardPool = game.add.group();
        // this.awardPool.enableBody = true;
        // this.awardPool.createMultiple(10, 'award');
        // this.awardPool.setAll('anchor.x', 0.5);
        // this.awardPool.setAll('anchor.y', 0.5);
        // this.awardPool.setAll('outOfBoundsKill', true);
        // this.awardPool.setAll('checkWorldBounds', true);

        // this.awardNextShotAt = 0;

        // this.awardShotDelay = 10000;


        /// ToDo 3: Add 4 animations.
        /// 1. Create the 'rightwalk' animation with frame rate = 8 by looping the frames 1 and 2
        this.player.animations.add('rightwalk', [1, 2], 8, true);

        /// 2. Create the 'leftwalk' animation with frame rate = 8 by looping the frames 3 and 4
        this.player.animations.add('leftwalk', [1, 2], 8, true);
        
        /// 3. Create the 'rightjump' animation with frame rate = 16 (frames 5 and 6 and no loop)
        //this.player.animations.add('rightjump', [5, 6], 16, false);
        
        /// 4. Create the 'leftjump' animation with frame rate = 16 (frames 7 and 8 and no loop)
        //this.player.animations.add('leftjump', [7, 8], 16, false);
        
        ///

        this.enemyPool = game.add.group();
        this.enemyPool.enableBody = true;
        this.enemyPool.createMultiple(50, 'enemy');
        this.enemyPool.setAll('anchor.x', 0.5);
        this.enemyPool.setAll('anchor.y', 0.5);
        this.enemyPool.setAll('outOfBoundsKill', true);
        this.enemyPool.setAll('checkWorldBounds', true);
        // this.enemyPool.forEach(function(enemy) {
        // enemy.animations.add('fly',[0, 1, 2], 20, true);
        // });
        this.nextEnemyAt = 0;
        this.enemyDelay = 5000;

        this.enemyBulletPool = game.add.group();
        this.enemyBulletPool.enableBody = true;
        this.enemyBulletPool.createMultiple(4, 'bullet');
        this.enemyBulletPool.setAll('anchor.x', 0.5);
        this.enemyBulletPool.setAll('anchor.y', 0.5);
        this.enemyBulletPool.setAll('outOfBoundsKill', true);
        this.enemyBulletPool.setAll('checkWorldBounds', true);

        this.enemyNextShotAt = 0;

        this.enemyShotDelay = 1000;

        this.boss1BulletPool = game.add.group();
        this.boss1BulletPool.enableBody = true;
        this.boss1BulletPool.createMultiple(10, 'bullet');
        this.boss1BulletPool.setAll('anchor.x', 0.5);
        this.boss1BulletPool.setAll('anchor.y', 0.5);
        this.boss1BulletPool.setAll('outOfBoundsKill', true);
        this.boss1BulletPool.setAll('checkWorldBounds', true);

        this.boss1NextShotAt = 0;

        this.boss1ShotDelay = 800;

        livingEnemies.length=0;


        /// Add a little yellow block :)
        //this.yellowBlock = game.add.sprite(200, 320, 'block1');
        //this.yellowBlock.animations.add('Yblockanim', [0, 1, 2, 3], 8,  true);
        //game.physics.arcade.enable(this.yellowBlock);
        //this.yellowBlock.body.immovable = true;
        
        /// Add a little dark blue block ;)
        this.blueBlock = game.add.sprite(422, 320, 'block2');
        this.blueBlock.animations.add('Bblockanim', [0, 1, 2, 3], 8,  true);
        game.physics.arcade.enable(this.blueBlock);
        this.blueBlock.kill();
        //this.blueBlock.body.immovable = true;
        

        /// Particle
        this.emitter = game.add.emitter(422, 320, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 500;


        /// Add floor
        this.floor = game.add.sprite(0, game.height - 37, 'ground'); 
        game.physics.arcade.enable(this.floor);
        this.floor.body.immovable = true;

        game.physics.arcade.enable(this.player);
        game.physics.arcade.enable(this.helper);
        game.physics.arcade.enable(this.boss1);
        game.physics.arcade.enable(this.award);

        // Add vertical gravity to the player
        //this.player.body.gravity.y = 500;

        //this.stopButton.game.add.button(610, 470, 'stopButton', this.stopOnClick, this);
        this.pauseButton = this.game.add.sprite(610, 470, 'pauseButton');

        this.volumeUpButton = game.add.button(570, 474, 'volumeUpButton', this.volumeUp, this);
        this.volumeDownButton = game.add.button(530, 474, 'volumeDownButton', this.VolumeDown, this);

        // this.volumeUpButton = this.game.add.sprite(570, 474, 'volumeUpButton');
        // this.volumeDownButton = this.game.add.sprite(530, 474, 'volumeDownButton');

        this.hp1 = this.game.add.sprite(20, 470, 'hp');
        this.hp2 = this.game.add.sprite(50, 470, 'hp');
        this.hp3 = this.game.add.sprite(80, 470, 'hp');

        this.mp = game.add.text(180, 490, "MP:MAX", {font: '25px monospace', fill: '#000000', align: 'center'});
        this.mp.anchor.setTo(0.5);

        this.scoreDisplay = game.add.text(330, 490, "Score:"+score, {font: '25px monospace', fill: '#000000', align: 'center'});
        this.scoreDisplay.anchor.setTo(0.5);

    },
    // blockTween: function() {

    //     var yellowBlockOriginalX = this.yellowBlock.x;
    //     var yellowBlockOriginalY = this.yellowBlock.y;
    //     /// ToDo 4: Tween yellow block.
    //     ///     Add Tween here: game.add.tween(this.yellowBlock)....? 
    //     ///     Move block to 20px above its original place with duration 100 ms 
    //     ///     And move it back (yoyo function).
    //     if(this.player.body.touching.up){
    //         var ybtween = game.add.tween(this.yellowBlock);
    //         ybtween.to({y:yellowBlockOriginalY-20}, 100);
    //         ybtween.yoyo(true);
    //         ybtween.start();
    //     }

    //     ///
    // },

    blockParticle: function() {

        /// ToDo 5: Start our emitter.
        ///      1. We'll burst out all partice at once.
        ///      2. The particle's lifespan is 800 ms.
        ///      3. Set frequency to null since we will burst out all partice at once.
        ///      4. We'll launch 15 particle.
        this.emitter.start(true, 800, null, 15);

        ///
    },
    update: function() {
        this.pauseButton.inputEnabled = true; 
        this.pauseButton.events.onInputUp.add(function() {
            game.paused = true;
        }, this);
        game.input.onDown.add(function() {
            if (game.paused) {
            game.paused = false;
            }
        }, this);
        this.pauseButton.fixedToCamera = true;

        // this.volumeUpButton.inputEnabled = true; 
        // this.volumeUpButton.events.onInputUp.add(function() {
        //     bgm.volume += 0.1;
        //     explosionBgm.volume += 0.1;
        // }, this);

        // this.volumeDownButton.inputEnabled = true; 
        // this.volumeDownButton.events.onInputUp.add(function() {
        //     bgm.volume -= 0.1;
        //     explosionBgm.volume -= 0.1;
        // }, this);

        this.firstBackground.tilePosition.y += 1;

        /// ToDo 6: Add collision 
        /// 1. Add collision between player and walls
        
        /// 2. Add collision between player and floor
        game.physics.arcade.collide(this.player, this.floor);
        game.physics.arcade.collide(this.helper, this.floor);
        
        /// 3. Add collision between player and yellowBlock and add trigger animation "blockTween"
        //game.physics.arcade.collide(this.player, this.yellowBlock, this.blockTween, null, this);
        
        /// 4. Add collision between player and blueBlock and add trigger animation "blockParticle"
        game.physics.arcade.collide(this.player, this.blueBlock, this.blockParticle, null, this);
        
        ///

        // Play the animation.
        //this.yellowBlock.animations.play('Yblockanim');
        this.blueBlock.animations.play('Bblockanim');


        if (!this.player.inWorld) { this.playerDie();}
        this.movePlayer();

        if (game.input.keyboard.isDown(Phaser.Keyboard.Z)) {
            this.fire();
        }
        if (game.input.keyboard.isDown(Phaser.Keyboard.X)) {
            this.skill();
        }
        this.helperFire();
        
        game.physics.arcade.overlap(this.bulletPool, this.enemyPool, this.enemyHit, null, this);
        game.physics.arcade.overlap(this.skillPool, this.enemyPool, this.enemyHit, null, this);
        game.physics.arcade.overlap(this.bulletPool, this.boss1, this.boss1Hit, null, this);
        game.physics.arcade.overlap(this.skillPool, this.boss1, this.boss1Hit, null, this);
        game.physics.arcade.overlap(this.player, this.enemyPool, this.playerHit, null, this);
        game.physics.arcade.overlap(this.player, this.boss1, this.playerHit, null, this);
        game.physics.arcade.overlap(this.helperBulletPool, this.enemyPool, this.enemyHit, null, this);
        game.physics.arcade.overlap(this.helperBulletPool, this.boss1, this.boss1Hit, null, this);
        
        if (this.nextEnemyAt<game.time.now && this.enemyPool.countDead()>0) {
            this.nextEnemyAt = game.time.now + this.enemyDelay;
            var enemy = this.enemyPool.getFirstExists(false);
            enemy.reset(game.rnd.integerInRange(50, 600), 0);
            enemy.body.velocity.y = game.rnd.integerInRange(80, 150);
            //enemy.play('fly');

            this.enemyPool.forEachAlive(function(alien){

                // put every living enemy in an array
                livingEnemies.push(alien);
            });
        }
        if(this.enemyKilledCount==3 && !this.boss1.alive){
            this.boss1.revive();
            livingEnemies.push(this.boss1);
        }
        if(this.boss1.alive){
            this.boss1.body.velocity.x = game.rnd.integerInRange(-500, 500);
            this.boss1.body.velocity.y = game.rnd.integerInRange(-10, 10);
        }

        this.enemyFire();
        this.boss1Fire();
        game.physics.arcade.overlap(this.player, this.enemyBulletPool, this.enemyBulletHit, null, this);
        game.physics.arcade.overlap(this.player, this.boss1BulletPool, this.boss1BulletHit, null, this);
        //console.log("here");
        game.physics.arcade.overlap(this.player, this.award, this.getAward, null, this);
        //console.log("isAwarded:"+isAwarded);
        if(isAwarded === true || score>=1200){
            // this.instruction1 = game.add.text(330, 150, 'Level up after 5 seconds, please get the award in time!', {font: '20px monospace', fill: '#fff', align: 'center'});
            // this.instruction1.anchor.setTo(0.5);
            // this.instExpire = game.time.now + 5000;
            console.log("HEY");
            console.log("isAwarded:"+isAwarded);
            game.state.start('Final');
        }

        // if (isAwarded == true) {
        //     if(game.time.now>this.instExpire){
        //         this.instruction1.destroy();
        //         game.stage.start('Final');
        //     }
        // }
    }, 
    playerDie: function() { game.state.start('Main');},

    /// ToDo 7: Finish the 4 animation part.
    movePlayer: function() {
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -200;
            this.helper.body.velocity.x = -200;
            this.player.facingLeft = true;

            /// 1. Play the animation 'leftwalk'
            this.player.animations.play('leftwalk');

            ///
        }
        if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 200;
            this.helper.body.velocity.x = 200;
            this.player.facingLeft = false;

            /// 2. Play the animation 'rightwalk' 
            this.player.animations.play('rightwalk');

            ///
        }    

        // If the up arrow key is pressed, And the player is on the ground.
        if (this.cursor.up.isDown) { 
            this.player.body.velocity.y = -200;
            this.helper.body.velocity.y = -200;
            // if(this.player.body.touching.down){
            //     //Move the player upward (jump)
            //     if(this.player.facingLeft) {
            //         /// 3. Play the 'leftjump' animation
            //         this.player.animations.play('leftjump');

            //         ///
            //     }else {
            //         /// 4. Play the 'rightjump' animation
            //         this.player.animations.play('rightwalk');

            //         ///
            //     }
            //     this.player.body.velocity.y = -350;
            // }
        }
        if (this.cursor.down.isDown) { 
            this.player.body.velocity.y = 200;
            this.helper.body.velocity.y = 200;
        }
        // If neither the right or left arrow key is pressed
        if(!this.cursor.left.isDown && !this.cursor.right.isDown && !this.cursor.up.isDown && !this.cursor.down.isDown) {
            // Stop the player 
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;
            this.helper.body.velocity.x = 0;
            this.helper.body.velocity.y = 0;
        
            if(this.player.facingLeft) {
                // Change player frame to 3 (Facing left)
                this.player.frame = 3;
            }else {
                // Change player frame to 1 (Facing right)
                this.player.frame = 3;
            }

            // Stop the animation
            this.player.animations.stop();
        }    
    },
    fire: function() { 
        if (!this.player.alive || this.nextShotAt>game.time.now) {
          return;
        }
        if (this.bulletPool.countDead()==0) {
          return;
        }
        this.nextShotAt = game.time.now + this.shotDelay;
        
        var bullet = this.bulletPool.getFirstExists(false);
        bullet.reset(this.player.x+25, this.player.y-10);
        bullet.body.velocity.y = -500;
    },
    skill: function() { 
        if (!this.player.alive || this.skillNextShotAt>game.time.now) {
          return;
        }
        if (this.skillPool.countDead()==0) {
          return;
        }
        this.skillNextShotAt = game.time.now + this.skillShotDelay;
        
        var skillBullet = this.skillPool.getFirstExists(false);
        //skillBullet.reset(this.player.x+25, this.player.y-10);
        //skillBullet.body.velocity.y = -500;
        if (skillBullet && livingEnemies.length > 0)
        {
            var shooter;

            for(var i=0; i<livingEnemies.length-1; i++){
                shooter=livingEnemies[i];

                // And fire the bullet from this enemy
                skillBullet.reset(this.player.x+25, this.player.y+25);

                game.physics.arcade.moveToObject(skillBullet,shooter,500);
                firingTimer = game.time.now + 2000;
            }
        }
    },
    helperFire: function() { 
        if (!this.helper.alive || this.helperNextShotAt>game.time.now) {
          return;
        }
        if (this.helperBulletPool.countDead()==0) {
          return;
        }
        this.helperNextShotAt = game.time.now + this.helperShotDelay;
        
        var helperBullet = this.helperBulletPool.getFirstExists(false);
        helperBullet.reset(this.helper.x+10, this.helper.y-10);
        helperBullet.body.velocity.y = -400;
    },
    boss1Fire: function() { 
        if (!this.boss1.alive || this.boss1NextShotAt>game.time.now) {
          return;
        }
        if (this.boss1BulletPool.countDead()==0) {
          return;
        }
        this.boss1rNextShotAt = game.time.now + this.boss1ShotDelay;
        
        var boss1Bullet = this.boss1BulletPool.getFirstExists(false);
        boss1Bullet.reset(this.boss1.x+25, this.boss1.y+25);
        game.physics.arcade.moveToObject(boss1Bullet,this.player,500);
    },
    enemyFire: function() { 
        if (!this.player.alive || this.enemyNextShotAt>game.time.now) {
          return;
        }
        if (this.enemyBulletPool.countDead()==0) {
          return;
        }
        this.enemyNextShotAt = game.time.now + this.enemyShotDelay;
        
        var enemyBullet = this.enemyBulletPool.getFirstExists(false);
        // enemyBullet.reset(enemy.x, enemy.y+20);
        // enemyBullet.body.velocity.y = game.rnd.integerInRange(70, 85);


        if (enemyBullet && livingEnemies.length > 0)
        {
            
            var random=game.rnd.integerInRange(0,livingEnemies.length-1);

            // randomly select one of them
            var shooter=livingEnemies[random];
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x+25, shooter.body.y+25);

            game.physics.arcade.moveToObject(enemyBullet,this.player,220);
            firingTimer = game.time.now + 2000;
        }
    },
    enemyHit: function(bullet, enemy) {
        explosionBgm.play();
        bullet.kill();
        enemy.kill();
        this.enemyKilledCount++;
        score+=30;
        this.scoreDisplay.text = "Score:"+score;
      
        var explosion = game.add.sprite(enemy.x, enemy.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);
    },
    boss1Hit: function(bullet, enemy) {
        explosionBgm.play();
        bullet.kill();
        enemy.kill();
        this.enemyKilledCount++;
        score+=1000;
        this.scoreDisplay.text = "Score:"+score;
      
        var explosion = game.add.sprite(enemy.x, enemy.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);

        //console.log("awardDisplay");
        this.award.revive();
        this.award.body.x = this.boss1.body.x;
        this.award.body.y = this.boss1.body.y;
    },
    playerHit: function(player, enemy) {
        explosionBgm.play();
        enemy.kill();
        this.enemyKilledCount++;
        score-=50;
        this.scoreDisplay.text = "Score:"+score;

        var explosion = game.add.sprite(player.x+30, player.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);

        player.life--;
        //console.log(player.life);
        if(player.life>0){
            player.x = game.width/2-20;
            player.y = game.height/2+150;
            this.helper.x = game.width/2+40;
            this.helper.y = game.height/2+155;
            if(player.life==2)  this.hp3.destroy();
            else if(player.life==1) this.hp2.destroy();
        }
        else{
            this.hp1.destroy();
            player.kill();
            game.state.start('Lose');
        }
    },
    enemyBulletHit: function(player, enemyBullet) {
        explosionBgm.play();
        enemyBullet.kill();
        score-=5;
        this.scoreDisplay.text = "Score:"+score;
      
        var explosion = game.add.sprite(player.x+30, player.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);

        player.life--;
        //console.log(player.life);
        if(player.life>0){
            player.x = game.width/2-20;
            player.y = game.height/2+150;
            this.helper.x = game.width/2+40;
            this.helper.y = game.height/2+155;
            if(player.life==2)  this.hp3.destroy();
            else if(player.life==1) this.hp2.destroy();
        }
        else{
            this.hp1.destroy();
            player.kill();
            game.state.start('Lose');
        }
    },
    boss1BulletHit: function(player, enemyBullet) {
        explosionBgm.play();
        enemyBullet.kill();
        score-=230;
        this.scoreDisplay.text = "Score:"+score;
      
        var explosion = game.add.sprite(player.x+30, player.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);

        player.life--;
        //console.log(player.life);
        if(player.life>0){
            player.x = game.width/2-20;
            player.y = game.height/2+150;
            this.helper.x = game.width/2+40;
            this.helper.y = game.height/2+155;
            if(player.life==2)  this.hp3.destroy();
            else if(player.life==1) this.hp2.destroy();
        }
        else{
            this.hp1.destroy();
            player.kill();
            game.state.start('Lose');
        }
    },
    getAward: function(player, myAward) {
        console.log("getAward");
        myAward.kill();
        console.log("killed");
        isAwarded = true;
        console.log("isAwarded:"+isAwarded);
    },
    volumeUp: function() {
        if(bgm.volume<1){
            bgm.volume += 0.1;
            explosionBgm.volume += 0.1;
        }
    },
    volumeDown: function() {
        if(bgm.volume>0){
            bgm.volume -= 0.1;
            explosionBgm.volume -= 0.1;
        }
    }
};

var Final = {
    preload: function() {

    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        //game.add.image(0, 0, 'background'); 
        this.secondBackground = game.add.tileSprite(0, 0, 650, 3000, 'secondBackground');

        /// ToDo 2: How can we enable physics in Phaser? (Mode : ARCADE)

        ///

        game.physics.startSystem(Phaser.Physics.ARCADE);

        game.renderer.renderSession.roundPixels = true;

        this.cursor = game.input.keyboard.createCursorKeys();
        
        this.player = game.add.sprite(game.width/2-20, game.height/2+150, 'player');
        this.player.life = 3;
        //score = 0;
        this.enemyKilledCount = 0;
        this.player.facingLeft = false;

        if(isAwarded == true)   this.helper = game.add.sprite(game.width/2+40, game.height/2+155, 'helper');

        //this.award = game.add.sprite(10000, 10000, 'award');

        this.boss2 = game.add.sprite(game.width/2-40, 10, 'boss');
        this.boss2.kill();


        this.bulletPool = game.add.group();
        this.bulletPool.enableBody = true;
        this.bulletPool.createMultiple(1000, 'bullet');
        this.bulletPool.setAll('anchor.x', 0.5);
        this.bulletPool.setAll('anchor.y', 0.5);
        this.bulletPool.setAll('outOfBoundsKill', true);
        this.bulletPool.setAll('checkWorldBounds', true);

        this.nextShotAt = 0;
        this.shotDelay = 100;

        if(isAwarded == true){
            this.skillPool = game.add.group();
            this.skillPool.enableBody = true;
            this.skillPool.createMultiple(1000, 'skill');
            this.skillPool.setAll('anchor.x', 0.5);
            this.skillPool.setAll('anchor.y', 0.5);
            this.skillPool.setAll('outOfBoundsKill', true);
            this.skillPool.setAll('checkWorldBounds', true);

            this.skillNextShotAt = 0;
            this.skillShotDelay = 100;

            this.helperBulletPool = game.add.group();
            this.helperBulletPool.enableBody = true;
            this.helperBulletPool.createMultiple(50, 'bullet');
            this.helperBulletPool.setAll('anchor.x', 0.5);
            this.helperBulletPool.setAll('anchor.y', 0.5);
            this.helperBulletPool.setAll('outOfBoundsKill', true);
            this.helperBulletPool.setAll('checkWorldBounds', true);

            this.helperNextShotAt = 0;
            this.helperShotDelay = 500;
        }


        /// ToDo 3: Add 4 animations.
        /// 1. Create the 'rightwalk' animation with frame rate = 8 by looping the frames 1 and 2
        this.player.animations.add('rightwalk', [1, 2], 8, true);

        /// 2. Create the 'leftwalk' animation with frame rate = 8 by looping the frames 3 and 4
        this.player.animations.add('leftwalk', [1, 2], 8, true);
        
        /// 3. Create the 'rightjump' animation with frame rate = 16 (frames 5 and 6 and no loop)
        //this.player.animations.add('rightjump', [5, 6], 16, false);
        
        /// 4. Create the 'leftjump' animation with frame rate = 16 (frames 7 and 8 and no loop)
        //this.player.animations.add('leftjump', [7, 8], 16, false);
        
        ///

        this.enemyPool = game.add.group();
        this.enemyPool.enableBody = true;
        this.enemyPool.createMultiple(50, 'enemy');
        this.enemyPool.setAll('anchor.x', 0.5);
        this.enemyPool.setAll('anchor.y', 0.5);
        this.enemyPool.setAll('outOfBoundsKill', true);
        this.enemyPool.setAll('checkWorldBounds', true);
        // this.enemyPool.forEach(function(enemy) {
        // enemy.animations.add('fly',[0, 1, 2], 20, true);
        // });
        this.nextEnemyAt = 0;
        this.enemyDelay = 200;

        this.enemyBulletPool = game.add.group();
        this.enemyBulletPool.enableBody = true;
        this.enemyBulletPool.createMultiple(10, 'bullet');
        this.enemyBulletPool.setAll('anchor.x', 0.5);
        this.enemyBulletPool.setAll('anchor.y', 0.5);
        this.enemyBulletPool.setAll('outOfBoundsKill', true);
        this.enemyBulletPool.setAll('checkWorldBounds', true);

        this.enemyNextShotAt = 0;

        this.enemyShotDelay = 700;

        this.boss2BulletPool = game.add.group();
        this.boss2BulletPool.enableBody = true;
        this.boss2BulletPool.createMultiple(30, 'bullet');
        this.boss2BulletPool.setAll('anchor.x', 0.5);
        this.boss2BulletPool.setAll('anchor.y', 0.5);
        this.boss2BulletPool.setAll('outOfBoundsKill', true);
        this.boss2BulletPool.setAll('checkWorldBounds', true);

        this.boss2NextShotAt = 0;

        this.boss2ShotDelay = 500;

        livingEnemies.length=0;


        /// Add a little yellow block :)
        //this.yellowBlock = game.add.sprite(200, 320, 'block1');
        //this.yellowBlock.animations.add('Yblockanim', [0, 1, 2, 3], 8,  true);
        //game.physics.arcade.enable(this.yellowBlock);
        //this.yellowBlock.body.immovable = true;
        
        /// Add a little dark blue block ;)
        this.blueBlock = game.add.sprite(422, 320, 'block2');
        this.blueBlock.animations.add('Bblockanim', [0, 1, 2, 3], 8,  true);
        game.physics.arcade.enable(this.blueBlock);
        this.blueBlock.kill();
        //this.blueBlock.body.immovable = true;
        

        /// Particle
        this.emitter = game.add.emitter(422, 320, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 500;


        /// Add floor
        this.floor = game.add.sprite(0, game.height - 37, 'ground'); 
        game.physics.arcade.enable(this.floor);
        this.floor.body.immovable = true;

        game.physics.arcade.enable(this.player);
        if(isAwarded == true)   game.physics.arcade.enable(this.helper);
        game.physics.arcade.enable(this.boss2);
        //game.physics.arcade.enable(this.award);

        // Add vertical gravity to the player
        //this.player.body.gravity.y = 500;

        //this.stopButton.game.add.button(610, 470, 'stopButton', this.stopOnClick, this);
        this.pauseButton = this.game.add.sprite(610, 470, 'pauseButton');

        this.volumeUpButton = game.add.button(570, 474, 'volumeUpButton', this.volumeUp, this);
        this.volumeDownButton = game.add.button(530, 474, 'volumeDownButton', this.VolumeDown, this);

        // this.volumeUpButton = this.game.add.sprite(570, 474, 'volumeUpButton');
        // this.volumeDownButton = this.game.add.sprite(530, 474, 'volumeDownButton');

        this.hp1 = this.game.add.sprite(20, 470, 'hp');
        this.hp2 = this.game.add.sprite(50, 470, 'hp');
        this.hp3 = this.game.add.sprite(80, 470, 'hp');

        if(isAwarded == true)   this.mp = game.add.text(180, 490, "MP:MAX", {font: '25px monospace', fill: '#000000', align: 'center'});
        else    this.mp = game.add.text(180, 490, "MP:0", {font: '25px monospace', fill: '#000000', align: 'center'});
        this.mp.anchor.setTo(0.5);

        this.scoreDisplay = game.add.text(330, 490, "Score:"+score, {font: '25px monospace', fill: '#000000', align: 'center'});
        this.scoreDisplay.anchor.setTo(0.5);

    },
    // blockTween: function() {

    //     var yellowBlockOriginalX = this.yellowBlock.x;
    //     var yellowBlockOriginalY = this.yellowBlock.y;
    //     /// ToDo 4: Tween yellow block.
    //     ///     Add Tween here: game.add.tween(this.yellowBlock)....? 
    //     ///     Move block to 20px above its original place with duration 100 ms 
    //     ///     And move it back (yoyo function).
    //     if(this.player.body.touching.up){
    //         var ybtween = game.add.tween(this.yellowBlock);
    //         ybtween.to({y:yellowBlockOriginalY-20}, 100);
    //         ybtween.yoyo(true);
    //         ybtween.start();
    //     }

    //     ///
    // },

    blockParticle: function() {

        /// ToDo 5: Start our emitter.
        ///      1. We'll burst out all partice at once.
        ///      2. The particle's lifespan is 800 ms.
        ///      3. Set frequency to null since we will burst out all partice at once.
        ///      4. We'll launch 15 particle.
        this.emitter.start(true, 800, null, 15);

        ///
    },
    update: function() {
        this.pauseButton.inputEnabled = true; 
        this.pauseButton.events.onInputUp.add(function() {
            game.paused = true;
        }, this);
        game.input.onDown.add(function() {
            if (game.paused) {
            game.paused = false;
            }
        }, this);
        this.pauseButton.fixedToCamera = true;

        // this.volumeUpButton.inputEnabled = true; 
        // this.volumeUpButton.events.onInputUp.add(function() {
        //     bgm.volume += 0.1;
        //     explosionBgm.volume += 0.1;
        // }, this);

        // this.volumeDownButton.inputEnabled = true; 
        // this.volumeDownButton.events.onInputUp.add(function() {
        //     bgm.volume -= 0.1;
        //     explosionBgm.volume -= 0.1;
        // }, this);

        this.secondBackground.tilePosition.y += 1;

        /// ToDo 6: Add collision 
        /// 1. Add collision between player and walls
        
        /// 2. Add collision between player and floor
        game.physics.arcade.collide(this.player, this.floor);
        if(isAwarded == true)   game.physics.arcade.collide(this.helper, this.floor);
        
        /// 3. Add collision between player and yellowBlock and add trigger animation "blockTween"
        //game.physics.arcade.collide(this.player, this.yellowBlock, this.blockTween, null, this);
        
        /// 4. Add collision between player and blueBlock and add trigger animation "blockParticle"
        game.physics.arcade.collide(this.player, this.blueBlock, this.blockParticle, null, this);
        
        ///

        // Play the animation.
        //this.yellowBlock.animations.play('Yblockanim');
        this.blueBlock.animations.play('Bblockanim');


        if (!this.player.inWorld) { this.playerDie();}
        this.movePlayer();

        if (game.input.keyboard.isDown(Phaser.Keyboard.Z)) {
            this.fire();
        }
        if (game.input.keyboard.isDown(Phaser.Keyboard.X) && isAwarded == true) {
            this.skill();
        }
        if(isAwarded == true)   this.helperFire();
        
        game.physics.arcade.overlap(this.bulletPool, this.enemyPool, this.enemyHit, null, this);
        if(isAwarded == true)   game.physics.arcade.overlap(this.skillPool, this.enemyPool, this.enemyHit, null, this);
        game.physics.arcade.overlap(this.bulletPool, this.boss2, this.boss2Hit, null, this);
        if(isAwarded == true)   game.physics.arcade.overlap(this.skillPool, this.boss2, this.boss2Hit, null, this);
        game.physics.arcade.overlap(this.player, this.enemyPool, this.playerHit, null, this);
        game.physics.arcade.overlap(this.player, this.boss2, this.playerHit, null, this);
        if(isAwarded == true)   game.physics.arcade.overlap(this.helperBulletPool, this.enemyPool, this.enemyHit, null, this);
        if(isAwarded == true)   game.physics.arcade.overlap(this.helperBulletPool, this.boss2, this.boss2Hit, null, this);
        
        if (this.nextEnemyAt<game.time.now && this.enemyPool.countDead()>0) {
            this.nextEnemyAt = game.time.now + this.enemyDelay;
            var enemy = this.enemyPool.getFirstExists(false);
            enemy.reset(game.rnd.integerInRange(50, 600), 0);
            enemy.body.velocity.y = game.rnd.integerInRange(200, 500);
            //enemy.play('fly');

            this.enemyPool.forEachAlive(function(alien){

                // put every living enemy in an array
                livingEnemies.push(alien);
            });
        }
        //console.log("enemyKilledCount"+this.enemyKilledCount);
        if(this.enemyKilledCount==20 && !this.boss2.alive){
            this.boss2.revive();
            livingEnemies.push(this.boss2);
        }
        if(this.boss2.alive){
            this.boss2.body.velocity.x = game.rnd.integerInRange(-800, 800);
            this.boss2.body.velocity.y = game.rnd.integerInRange(-10, 10);
        }

        this.enemyFire();
        this.boss2Fire();
        game.physics.arcade.overlap(this.player, this.enemyBulletPool, this.enemyBulletHit, null, this);
        game.physics.arcade.overlap(this.player, this.boss2BulletPool, this.boss2BulletHit, null, this);

        //game.physics.arcade.overlap(this.player, this.award, this.getAward, null, this);
        if(isBossKilled){
            // this.instruction1 = game.add.text(330, 150, 'Level up after 5 seconds, please get the award in time!', {font: '20px monospace', fill: '#fff', align: 'center'});
            // this.instruction1.anchor.setTo(0.5);
            // this.instExpire = game.time.now + 5000;
            game.state.start('Win'); 
        }
        // if (this.instruction1.exists && game.time.now>this.instExpire) {
        //     this.instruction1.destroy();
        //     game.state.start('Final');
        // }
    }, 
    playerDie: function() { game.state.start('Main');},

    /// ToDo 7: Finish the 4 animation part.
    movePlayer: function() {
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -200;
            if(isAwarded == true)   this.helper.body.velocity.x = -200;
            this.player.facingLeft = true;

            /// 1. Play the animation 'leftwalk'
            this.player.animations.play('leftwalk');

            ///
        }
        if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 200;
            if(isAwarded == true)   this.helper.body.velocity.x = 200;
            this.player.facingLeft = false;

            /// 2. Play the animation 'rightwalk' 
            this.player.animations.play('rightwalk');

            ///
        }    

        // If the up arrow key is pressed, And the player is on the ground.
        if (this.cursor.up.isDown) { 
            this.player.body.velocity.y = -200;
            if(isAwarded == true)   this.helper.body.velocity.y = -200;
            // if(this.player.body.touching.down){
            //     //Move the player upward (jump)
            //     if(this.player.facingLeft) {
            //         /// 3. Play the 'leftjump' animation
            //         this.player.animations.play('leftjump');

            //         ///
            //     }else {
            //         /// 4. Play the 'rightjump' animation
            //         this.player.animations.play('rightwalk');

            //         ///
            //     }
            //     this.player.body.velocity.y = -350;
            // }
        }
        if (this.cursor.down.isDown) { 
            this.player.body.velocity.y = 200;
            if(isAwarded == true)   this.helper.body.velocity.y = 200;
        }
        // If neither the right or left arrow key is pressed
        if(!this.cursor.left.isDown && !this.cursor.right.isDown && !this.cursor.up.isDown && !this.cursor.down.isDown) {
            // Stop the player 
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;
            if(isAwarded == true)   this.helper.body.velocity.x = 0;
            if(isAwarded == true)   this.helper.body.velocity.y = 0;
        
            if(this.player.facingLeft) {
                // Change player frame to 3 (Facing left)
                this.player.frame = 3;
            }else {
                // Change player frame to 1 (Facing right)
                this.player.frame = 3;
            }

            // Stop the animation
            this.player.animations.stop();
        }    
    },
    fire: function() { 
        if (!this.player.alive || this.nextShotAt>game.time.now) {
          return;
        }
        if (this.bulletPool.countDead()==0) {
          return;
        }
        this.nextShotAt = game.time.now + this.shotDelay;
        
        var bullet = this.bulletPool.getFirstExists(false);
        bullet.reset(this.player.x+25, this.player.y-10);
        bullet.body.velocity.y = -500;
    },
    skill: function() { 
        if (!this.player.alive || this.skillNextShotAt>game.time.now) {
          return;
        }
        if (this.skillPool.countDead()==0) {
          return;
        }
        this.skillNextShotAt = game.time.now + this.skillShotDelay;
        
        var skillBullet = this.skillPool.getFirstExists(false);
        //skillBullet.reset(this.player.x+25, this.player.y-10);
        //skillBullet.body.velocity.y = -500;
        if (skillBullet && livingEnemies.length > 0)
        {
            var shooter;

            for(var i=0; i<livingEnemies.length-1; i++){
                shooter=livingEnemies[i];

                // And fire the bullet from this enemy
                skillBullet.reset(this.player.x+25, this.player.y+25);

                game.physics.arcade.moveToObject(skillBullet,shooter,500);
                firingTimer = game.time.now + 2000;
            }
        }
    },
    helperFire: function() { 
        if (!this.helper.alive || this.helperNextShotAt>game.time.now) {
          return;
        }
        if (this.helperBulletPool.countDead()==0) {
          return;
        }
        this.helperNextShotAt = game.time.now + this.helperShotDelay;
        
        var helperBullet = this.helperBulletPool.getFirstExists(false);
        helperBullet.reset(this.helper.x+10, this.helper.y-10);
        helperBullet.body.velocity.y = -400;
    },
    boss2Fire: function() { 
        if (!this.boss2.alive || this.boss2NextShotAt>game.time.now) {
          return;
        }
        if (this.boss2BulletPool.countDead()==0) {
          return;
        }
        this.boss2rNextShotAt = game.time.now + this.boss2ShotDelay;
        
        var boss2Bullet = this.boss2BulletPool.getFirstExists(false);
        boss2Bullet.reset(this.boss2.x+25, this.boss2.y+25);
        game.physics.arcade.moveToObject(boss2Bullet,this.player,850);
    },
    enemyFire: function() { 
        if (!this.player.alive || this.enemyNextShotAt>game.time.now) {
          return;
        }
        if (this.enemyBulletPool.countDead()==0) {
          return;
        }
        this.enemyNextShotAt = game.time.now + this.enemyShotDelay;
        
        var enemyBullet = this.enemyBulletPool.getFirstExists(false);
        // enemyBullet.reset(enemy.x, enemy.y+20);
        // enemyBullet.body.velocity.y = game.rnd.integerInRange(70, 85);


        if (enemyBullet && livingEnemies.length > 0)
        {
            
            var random=game.rnd.integerInRange(0,livingEnemies.length-1);

            // randomly select one of them
            var shooter=livingEnemies[random];
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x+25, shooter.body.y+25);

            game.physics.arcade.moveToObject(enemyBullet,this.player,650);
            firingTimer = game.time.now + 2000;
        }
    },
    enemyHit: function(bullet, enemy) {
        explosionBgm.play();
        bullet.kill();
        enemy.kill();
        this.enemyKilledCount++;
        score+=30;
        this.scoreDisplay.text = "Score:"+score;
      
        var explosion = game.add.sprite(enemy.x, enemy.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);
    },
    boss2Hit: function(bullet, enemy) {
        explosionBgm.play();
        bullet.kill();
        enemy.kill();
        this.enemyKilledCount++;
        score+=1000;
        this.scoreDisplay.text = "Score:"+score;
      
        var explosion = game.add.sprite(enemy.x, enemy.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);

        //this.awardDisplay(enemy.x, enemy.y);
        game.state.start('Win');
    },
    playerHit: function(player, enemy) {
        explosionBgm.play();
        enemy.kill();
        this.enemyKilledCount++;
        score-=50;
        this.scoreDisplay.text = "Score:"+score;

        var explosion = game.add.sprite(player.x+30, player.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);

        player.life--;
        //console.log(player.life);
        if(player.life>0){
            player.x = game.width/2-20;
            player.y = game.height/2+150;
            if(isAwarded == true)   this.helper.x = game.width/2+40;
            if(isAwarded == true)   this.helper.y = game.height/2+155;
            if(player.life==2)  this.hp3.destroy();
            else if(player.life==1) this.hp2.destroy();
        }
        else{
            this.hp1.destroy();
            player.kill();
            game.state.start('Lose');
        }
    },
    enemyBulletHit: function(player, enemyBullet) {
        explosionBgm.play();
        enemyBullet.kill();
        score-=5;
        this.scoreDisplay.text = "Score:"+score;
      
        var explosion = game.add.sprite(player.x+30, player.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);

        player.life--;
        //console.log(player.life);
        if(player.life>0){
            player.x = game.width/2-20;
            player.y = game.height/2+150;
            if(isAwarded == true)   this.helper.x = game.width/2+40;
            if(isAwarded == true)   this.helper.y = game.height/2+155;
            if(player.life==2)  this.hp3.destroy();
            else if(player.life==1) this.hp2.destroy();
        }
        else{
            this.hp1.destroy();
            player.kill();
            game.state.start('Lose');
        }
    },
    boss2BulletHit: function(player, enemyBullet) {
        explosionBgm.play();
        enemyBullet.kill();
        score-=230;
        this.scoreDisplay.text = "Score:"+score;
      
        var explosion = game.add.sprite(player.x+30, player.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);

        player.life--;
        //console.log(player.life);
        if(player.life>0){
            player.x = game.width/2-20;
            player.y = game.height/2+150;
            if(isAwarded == true)   this.helper.x = game.width/2+40;
            if(isAwarded == true)   this.helper.y = game.height/2+155;
            if(player.life==2)  this.hp3.destroy();
            else if(player.life==1) this.hp2.destroy();
        }
        else{
            this.hp1.destroy();
            player.kill();
            game.state.start('Lose');
        }
    },
    // awardDisplay: function(enemy_x, enemy_y) {
    //     this.award.x = enemy_x;
    //     this.award.y = enemy_y;
    //     isAwarded = false;
    // },
    // getAward: function() {
    //     this.award.kill();
    //     isAwarded = true;
    // },
    volumeUp: function() {
        if(bgm.volume<1){
            bgm.volume += 0.1;
            explosionBgm.volume += 0.1;
        }
    },
    volumeDown: function() {
        if(bgm.volume>0){
            bgm.volume -= 0.1;
            explosionBgm.volume -= 0.1;
        }
    }
};

var Lose = {
    preload: function() {
        this.menuButton = game.load.image('menu', 'assets/menuButton.png');
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'startBackground');
        this.menuButton.game.add.button(240, 360, 'menu', this.actionOnClick, this);
        // button.onInputOver.add(over, this);
        // button.onInputOut.add(out, this);
        // button.onInputUp.add(up, this);
        this.cursors = game.input.keyboard.createCursorKeys();
        this.title = game.add.text(330, 140, 'Game over! You lose!', {font: '55px monospace', fill: '#CC0000', align: 'center'});
        this.title.anchor.setTo(0.5);
        this.yourScore = game.add.text(240, 250, 'Your score:'+score, {font: '30px monospace', fill: '#000000', align: 'center'});
        this.yourScore.anchor.setTo(0.5);

        var name = prompt("Please enter your name");
        if (name != null) {
            firebase.database().ref('rank').push({
                score:score,
                name:name
            });
        }
    },
    update: function() {

    },
    actionOnClick: function() {
        bgm.stop();
        game.state.start('Menu');
    }
};

var Win = {
    preload: function() {
        this.menuButton = game.load.image('menu', 'assets/menuButton.png');
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'startBackground');
        this.menuButton.game.add.button(240, 360, 'menu', this.actionOnClick, this);
        // button.onInputOver.add(over, this);
        // button.onInputOut.add(out, this);
        // button.onInputUp.add(up, this);
        this.cursors = game.input.keyboard.createCursorKeys();
        this.title = game.add.text(330, 140, 'Game over! You win!', {font: '55px monospace', fill: '#CC0000', align: 'center'});
        this.title.anchor.setTo(0.5);
        this.yourScore = game.add.text(240, 250, 'Your score:'+score, {font: '30px monospace', fill: '#000000', align: 'center'});
        this.yourScore.anchor.setTo(0.5);

        var name = prompt("Please enter your name");
        if (name != null) {
            firebase.database().ref('rank').push({
                score:score,
                name:name
            });
        }
    },
    update: function() {

    },
    actionOnClick: function() {
        bgm.stop();
        game.state.start('Menu');
    }
};

var ScoreBoard = {
    preload: function() {
        this.menuButton = game.load.image('menu', 'assets/menuButton.png');
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'startBackground');
        this.menuButton.game.add.button(240, 420, 'menu', this.actionOnClick, this);

        this.cursors = game.input.keyboard.createCursorKeys();
        this.title = game.add.text(330, 140, 'ScoreBoard', {font: '80px monospace', fill: '#CC0000', align: 'center'});
        this.title.anchor.setTo(0.5);

        var postsRef = firebase.database().ref('rank').orderByChild('score').limitToLast(5);

        var total_name = [];
        var total_score = [];

        var spacing=0;
        var rank=1;

        postsRef.once('value')
            .then(function (snapshot) {
                snapshot.forEach(function(childSnapshot) {
                    total_name.push(childSnapshot.val().name);
                    total_score.push(childSnapshot.val().score);
                });
                for(var i=4; i>=0; i--){
                    game.add.text(150, 210+spacing, "#"+rank+". "+total_name[i]+" "+total_score[i], {font: '30px monospace', fill: '#000000', align: 'center'})
                    spacing+=40;
                    rank+=1;
                }
            })
            .catch(e => console.log(e.message));
    },
    update: function() {

    },
    actionOnClick: function() {
        bgm.stop();
        game.state.start('Menu');
    }
};

var game = new Phaser.Game(650, 500, Phaser.AUTO, 'canvas');
game.state.add('Boot', Boot);
game.state.add('Preloader', Preloader);
game.state.add('Menu', Menu);
game.state.add('Main', Main);
game.state.add('Final', Final);
game.state.add('Win', Win);
game.state.add('Lose', Lose);
game.state.add('ScoreBoard', ScoreBoard);
game.state.start('Boot');



